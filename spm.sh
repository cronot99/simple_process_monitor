#!/bin/bash

# Simple Process Monitor using PID
# Recommended Cols: PID NI PR %CPU %MEM VIRT RES SHR S TIME+ COMMAND RUID RUSER
# Refresh time: 3 seconds

OUTPUT_FILE=spm.log
SPM_CPU_FILE=spm_cpu.txt
SPM_MEM_FILE=spm_mem.txt
SPM_SWAP_FILE=spm_swap.txt
SPM_PID_FILE=spm_pid.txt
PIDS=
TOPRC_MD5=toprc.md5

# Function for set the time to each event
function add_time {

while read each_line
do
	if [[ $each_line == device* ]]
	then
		echo -e "time\t$each_line"
	elif [[ $each_line == PID* ]]
	then
		echo -e "TIME\t$each_line"
	elif [[ "$each_line" =~ ^[0-9]*\:.* ]]
	then
		CUR_TIME=$each_line
	else
		echo -e "$CUR_TIME\t$each_line"
	fi
done < $1
}

# Verify if top configuration file is deployed
cat $TOPRC_MD5 |
sed -e "s/\$HOME/$(echo $HOME | sed 's/\//\\\//g')/" |
md5sum -c --status

if [[ $? -ne 0 ]]
then
cat <<EOF
####
ERROR: The TOP configuration file is incorrect.
Verify that the file $HOME/.toprc it's the same provided by the application.
####
EOF
exit 1
fi

while getopts 'o:hp:' opt
do
	case $opt in
		o) OUTPUT_FILE=$OPTARG ;;
		p) PIDS=$OPTARG ;;
		h) cat <<EOF 
Simple Process Monitor
-o: Top Output File
-p: PIDs (max 20)
-h: Display this message
EOF
exit 0
;;
	\?) echo 'Opción no soportada.' 
		exit 0
		;;
	esac
done

top -b -p $PIDS > $OUTPUT_FILE &
TOP_PID=$!

while :
do
	echo 'Press q for exit'
	read -s -n 1 key

	if [[ $key = 'q' ]]
	then
		kill -3 $TOP_PID
		break
	fi
done

echo "Creating cpu report $SPM_CPU_FILE..."
echo -e "device\tus\tsy\tni\tid\twa\thi\tsi\tst" > $SPM_CPU_FILE
grep -e '^%Cpu\|^top' "$OUTPUT_FILE" | \
sed -e 's/^top[^0-9]*\([^ ]*\).*/\1/' \
-e 's/\([0-9]*,[0-9]*\)[^0-9]*/\1\t/g' \
-e 's/ [ ]*: [ ]*/\t/' >> $SPM_CPU_FILE

TMP=$(add_time $SPM_CPU_FILE)
echo "$TMP" > $SPM_CPU_FILE

echo "Creating memory report $SPM_MEM_FILE..."
echo -e "device\ttotal\tused\tfree\tbuffers" > $SPM_MEM_FILE
grep -e '^KiB Mem\|^top' "$OUTPUT_FILE" |
sed -e 's/^top[^0-9]*\([^ ]*\).*/\1/' \
-e 's/\(KiB[^:]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\).*/\1\t\2\t\3\t\4\t\5/' >> $SPM_MEM_FILE

TMP=$(add_time $SPM_MEM_FILE)
echo "$TMP" > $SPM_MEM_FILE

echo "Creating swap memory report $SPM_SWAP_FILE..."
echo -e "device\ttotal\tused\tfree\tcached mem" > $SPM_SWAP_FILE
grep -e '^KiB Swap\|^top' "$OUTPUT_FILE" |
sed -e 's/^top[^0-9]*\([^ ]*\).*/\1/' \
-e 's/\(KiB[^:]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\).*/\1\t\2\t\3\t\4\t\5/' >> $SPM_SWAP_FILE

TMP=$(add_time $SPM_SWAP_FILE)
echo "$TMP" > $SPM_SWAP_FILE

echo "Creating applications memory report $SPM_PID_FILE..."
echo -e "PID\tPR\tNI\tVIRT\tRES\tSHR\tS\t%CPU\t%MEM\tTIME+\tCOMMAND\tRUID\tRUSER" > $SPM_PID_FILE
grep -e '^[ ]*[0-9]\|^top' "$OUTPUT_FILE" |
sed -e 's/^top[^0-9]*\([^ ]*\).*/\1/' \
-e 's/ [ ]*/\t/g' -e 's/^[\t]*\([0-9]\)/\1/' >> $SPM_PID_FILE

TMP=$(add_time $SPM_PID_FILE)
echo "$TMP" > $SPM_PID_FILE

echo DONE
